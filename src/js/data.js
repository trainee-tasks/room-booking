const pageData = {
  hotelName: "Lotus Suite",
  discounts: [
    {
      name: "SUMMER25",
      rate: 25,
      rateInPercent: true
    },
    {
      name: "PROMO10",
      rate: 10,
      rateInPercent: true
    }
  ],
  room: {
    specs: [
      "TV",
      "Air Conditioning",
      "Wi-Fi",
      "Electronic Safe",
      "Tea/Coffee Machine",
      "Washer and Dryer"
    ],
    imageUrl: "../src/images/room-image.jpg",
    remaining: 11,
    size: "King",
    maxPerson: 2,
    types: [
      {
        id: "room1",
        name: "AC Room Only",
        details: "Room 1. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, magnam reprehenderit. Consequatur repudiandae, voluptates, optio animi dignissimos cum, natus ratione blanditiis vel sapiente exercitationem maiores eaque inventore necessitatibus at illum.",
        price: 14000,
        discountApplicable: true,
        refundable: false,
        maxQuantity: 5
      },
      {
        id: "room2",
        name: "Non AC Room With Breakfast",
        details: "Room 2. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, magnam reprehenderit. Consequatur repudiandae, voluptates, optio animi dignissimos cum, natus ratione blanditiis vel sapiente exercitationem maiores eaque inventore necessitatibus at illum.",
        price: 15000,
        discountApplicable: true,
        refundable: false,
        maxQuantity: 5
      },
      {
        id: "room3",
        name: "AC Room With Breakfast & Dinner",
        details: "Room 3. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, magnam reprehenderit. Consequatur repudiandae, voluptates, optio animi dignissimos cum, natus ratione blanditiis vel sapiente exercitationem maiores eaque inventore necessitatibus at illum.",
        price: 17000,
        discountApplicable: true,
        refundable: true,
        maxQuantity: 5
      },
    ]
  }
}