const CONSTANTS = {
  MAIN_CONTAINER_SELECTOR: "#mainContainer",
  CONTAINER_SELECTOR: "#container",
  CONTAINER_TEMPLATE: `
    <div id="container" class="container-room">
      <div class="container-room-head flexed nowrap">
        <div id="roomImage" class="room-image-container md">
          <div class="room-image-content flexed vr-center between wrap">
            <div class="room-status-container flexed vr-center">
              <span class="material-icons-round icon-value">favorite</span>
              <span class="icon-text">Most Loved</span>
            </div>
            <div class="rooms-remaining-container flexed vr-center">
              <span id="roomsRemain" class="rooms-remaining-value"><!-- Remaining Rooms --></span>
            </div>
          </div>
        </div>
        <div class="room-detail-container flexed col between">
          <div>
            <h2 class="hotel-name"><!-- Hotel Name --></h2>
            <div class="room-detail-container-row flexed nowrap between top">
              <p id="roomSpecs"><!-- Room Specs --></p>
              <button id="roomSpecsToggleBtn" class="btn">View <span>More</span></button>
            </div>
            <div class="room-detail-container-row flexed nowrap vr-center">
              <div class="flexed vr-center">
                <span class="material-icon material-icons-round">people</span>
                &nbsp;&nbsp;&nbsp;
                <span id="maxPerson" class="icon-text"><!-- Room Max Person Limit --></span>
              </div>
              <div class="flexed vr-center">
                <span class="material-icon material-icons-round">hotel</span>
                &nbsp;&nbsp;&nbsp;
                <span id="roomSize" class="icon-text"><!-- Room Size --></span>
              </div>
            </div>
          </div>
          <div class="discount-list-container flexed vr-center">
            <span class="material-icons-round">discount</span>
            &nbsp;&nbsp;&nbsp;
            <div id="discountList"><!-- Discounts List --></div>
          </div>
        </div>
      </div>
      <div class="room-list-container">
        <div id="roomList" class="room-list-content"><!-- Room List --></div>
        <button id="roomsContainerToggleBtn" class="btn"><span>Hide</span> All</button>
      </div>
    </div>
  `,
  // Room Image
  ROOM_IMAGE_CONTAINER_SELECTOR: "#roomImage",
  HOTEL_ROOM_REMAIN_SELECTOR: "#roomsRemain",
  // Hotel Room Details
  HOTEL_NAME_SELECTOR: ".hotel-name",
  HOTEL_ROOM_SPECS_CONTAINER_SELECTOR: "#roomSpecs",
  HOTEL_ROOM_SPECS_VIEW_MORE_BUTTON_SELECTOR: "#roomSpecsToggleBtn",
  HOTEL_ROOM_PERSON_LIMIT_SELECTOR: "#maxPerson",
  HOTEL_ROOM_SIZE_SELECTOR: "#roomSize",
  // Discount List
  HOTEL_ROOM_DISCOUNT_TEMPLATE: `
    <button id="" class="btn discount-btn">
      <span class="discount-name"><!-- Discount Name --></span>
    </button>
  `,
  HOTEL_ROOM_DISCOUNT_LIST_CONTAINER_SELECTOR: "#discountList",
  HOTEL_ROOM_DISCOUNT_BUTTON_SELECTOR: ".discount-btn",
  HOTEL_ROOM_DISCOUNT_NAME_SELECTOR: ".discount-name",
  // Rooms List
  HOTEL_ROOM_LIST_TEMPLATE: `
    <div class="room-list-content-row flexed between">
      <div class="room-list-content-col">
        <h3 class="room-name"><!-- Room Name --></h3>
        <p class="room-details"><!-- Room Details --></p>
        <button class="btn room-detail-view-button"><span>View</span> Details</button>
      </div>
      <div class="room-list-content-col">
        <div class="flexed centered">
          <button class="btn room-quantity-inc-btn flexed centered">
            <span class="material-icons-round">add</span>
          </button>
          <span class="room-current-quantity"><!-- Selected Rooms --></span>
          <button class="btn room-quantity-dec-btn flexed centered">
            <span class="material-icons-round">remove</span>
          </button>
        </div>
      </div>
      <div class="room-list-content-col">
        <div class="room-current-price-container flexed baseline right">
          <div style="display: flex; align-self: center;" >
            <span class="material-icons-round">currency_rupee</span>
          </div>
          <span class="room-current-price"><!-- Room Current Price --></span>
          <span>&nbsp;/</span>
          <span class="room-price-per-unit">night</span>
        </div>
        <div class="room-price-refundable-status-container flexed vr-center right">
          <span class="room-price-refundable-status"><!-- Refundable Status --></span>
          <span class="material-icon material-icons-round">info</span>
        </div>
      </div>
    </div>
  `,
  HOTEL_ROOM_LIST_CONTAINER_SELECTOR: "#roomList",
  HOTEL_ROOM_NAME_SELECTOR: ".room-name",
  HOTEL_ROOM_DETAILS_CONTAINER_SELECTOR: ".room-details",
  HOTEL_ROOM_DETAIL_VIEW_BUTTON_SELECTOR: ".room-detail-view-button",
  HOTEL_ROOM_QUANTITY_INCREMENT_BUTTON_SELECTOR: ".room-quantity-inc-btn",
  HOTEL_ROOM_CURRENT_QUANTITY_SELECTOR: ".room-current-quantity",
  HOTEL_ROOM_QUANTITY_DECREMENT_BUTTON_SELECTOR: ".room-quantity-dec-btn",
  HOTEL_ROOM_DISCOUNT_APPLIED_INDICATOR_TEMPLATE: `
    <p class="discount-indicator flexed centered">
      <span>*Applied</span>&nbsp;<span class="discount-name"><!-- Discount Name --></span>
    </p>
  `,
  HOTEL_ROOM_DISCOUNT_APPLIED_INDICATOR_SELECTOR: ".discount-indicator",
  HOTEL_ROOM_INITIAL_PRICE_CONTAINER_TEMPLATE: `
    <div class="room-initial-price-container flexed vr-center right">
      <p>
        <span>INR</span>
        <span class="room-initial-price"><!-- Room Initial Price --></span>
      </p>
      <span class="material-icon material-icons-round">info</span>
    </div>
  `,
  HOTEL_ROOM_INITIAL_PRICE_CONTAINER_SELECTOR: ".room-initial-price-container",
  HOTEL_ROOM_INITIAL_PRICE_SELECTOR: ".room-initial-price",
  HOTEL_ROOM_CURRENT_PRICE_SELECTOR: ".room-current-price",
  HOTEL_ROOM_REFUNDABLE_STATUS_SELECTOR: ".room-price-refundable-status",
  HOTEL_ROOM_LIST_VIEW_ALL_BUTTON_SELECTOR: "#roomsContainerToggleBtn",
  // Checkout Container
  CHECKOUT_CONTAINER_SELECTOR: "#checkoutContainer",
  CHECKOUT_CONTAINER_TEMPLATE: `
    <div id="checkoutContainer">
      <h2>Tarrif Details</h2>
      <div class="flexed gap-2">
        <div class="flexed gap">
          <span>Rooms</span>
          <span id="totalRoomCount"><!-- Total Room Count --></span>
        </div>
        <div class="flexed gap">
          <span>Adults</span>
          <span id="totalHeadCount"><!-- Total Head Count --></span>
        </div>
      </div>
      <div class="flexed col gap-1">
        <div class="flexed col">
          <label for="checkinDatetime">Check-in</label>
          <input
            class="datetime-input"
            type="date"
            id="checkinDatetime"
            min=""
            max=""
            value=""
          />
        </div>
        <div class="flexed col">
          <label for="checkoutDatetime">Check-out</label>
          <input
            class="datetime-input"
            type="date"
            id="checkoutDatetime"
            min=""
            max=""
            value=""
          />
        </div>
      </div>
      <div id="selectedRoomList"><!-- Selected Rooms List --></div>
      <div class="flexed col top gap-1">
        <div id="priceBreakDown" style="display: none;">
          <div class="flexed between gap">
            <span>Total Amount</span>
            <span id="totalInitialAmount"><!-- Total Initial Amount --></span>
          </div>
          <div class="flexed between gap">
            <span>Discount Amount</span>
            <div>
              <span>&minus;</span>
              <span id="totalDiscountAmount"><!-- Total Discount Amount --></span>
            </div>
          </div>
        </div>
        <div class="flexed between">
          <h3>Total Payable</h3>
          <h3>
            INR <span id="totalAmount"><!-- Total Amount --></span>
          </h3>
        </div>
        <button id="priceBreakDownToggle" class="btn"><span>View</span> All Details</button>
        <form id="checkoutForm" method="post">
          <button disabled type="submit" id="submitCheckoutForm" class="btn">Proceed To Payment</button>
        </form>
        <p id="termsAndConditions">
          By proceeding you agree to the <a href="#">Hotel Booking Policy</a>, <a href="#">Hotel Cancellation Policy</a> and
          <a href="#">Terms &amp; Conditions</a> of <span class="hotel-name"> <!-- Hotel Name --> </span>.
        </p>
      </div>
    </div>
  `,
  CHECKOUT_DISPLAY_TOTAL_ROOM_COUNT_SELECTOR: "#totalRoomCount",
  CHECKOUT_DISPLAY_TOTAL_HEAD_COUNT_SELECTOR: "#totalHeadCount",
  CHECKOUT_INPUT_CHECKIN_DATETIME_SELECTOR: "#checkinDatetime",
  CHECKOUT_INPUT_CHECKOUT_DATETIME_SELECTOR: "#checkoutDatetime",
  CHECKOUT_SELECTED_ROOM_LIST_CONTAINER_SELECTOR: "#selectedRoomList",
  CHECKOUT_SELECTED_ROOM_ID_SUFFIX: "Checkout",
  CHECKOUT_SELECTED_ROOM_CONTAINER_TEMPLATE: `
    <div class="row flexed between baseline gap">
      <div class="col">
        <h4>SEMI DELUXE</h4>
        <h4 class="room-name"><!-- Room Name --></h4>
      </div>
      <div>
        <span style="font-size: 20px">&times;</span>
        <select class="update-room-current-quantity"></select>
      </div>
      <div class="flexed gap vr-center">
        <div>
          <span>INR</span>
          <span class="room-current-price"><!-- Room Current Price --></span>
        </div>
        <button class="btn remove-room">&times;</button>
      </div>
    </div>
  `,
  CHECKOUT_INPUT_UPDATE_ROOM_CURRENT_QUANTITY_SELECTOR: ".update-room-current-quantity",
  CHECKOUT_INPUT_UPDATE_ROOM_CURRENT_QUANTITY_CONTAINER_TEMPLATE: `
    <option value=""><!-- Option Text --></option>
  `,
  CHECKOUT_REMOVE_ROOM_BUTTON_SELECTOR: ".remove-room",
  CHECKOUT_PRICE_BREAK_DOWN_CONTAINER_SELECTOR: "#priceBreakDown",
  CHECKOUT_PRICE_BREAK_DOWN_TOGGLE_BUTTON_SELECTOR: "#priceBreakDownToggle",
  CHECKOUT_DISPLAY_TOTAL_INITIAL_ROOM_PRICE_SELECTOR: "#totalInitialAmount",
  CHECKOUT_DISPLAY_TOTAL_DISCOUNT_AMOUNT_SELECTOR: "#totalDiscountAmount",
  CHECKOUT_DISPLAY_TOTAL_AMOUNT_SELECTOR: "#totalAmount",
  CHECKOUT_FORM_SELECTOR: "#checkoutForm",
  CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR: "#submitCheckoutForm",
  CHECKOUT_TERMS_AND_CONDITIONS_CONTAINER_SELECTOR: "#termsAndConditions"
}