const CONSTANTS = {
  MAIN_CONTAINER_SELECTOR: "#mainContainer",
  CONTAINER_SELECTOR: "#container",
  CONTAINER_TEMPLATE: `
    <div class="container col-xxl-10 shadow rounded-3 px-0" id="container">
      <div class="container-room-head row m-0">
        <div id="roomImage" class="col-lg-5 col-xxl-5">
          <div class="room-image-content row justify-content-between">
            <div class="room-status-container m-3 p-1 row col-4 rounded-pill bg-light text-body">
              <span class="material-icons-round icon-value text-danger col-3 pe-1">favorite</span>
              <span class="icon-text col text-truncate">Most Loved</span>
            </div>
            <div class="rooms-remaining-container m-3 p-1 row justify-content-end col-4 rounded-pill bg-light text-body">
              <span id="roomsRemain" class="rooms-remaining-value text-truncate"><!-- Remaining Rooms --></span>
            </div>
          </div>
        </div>
        <div class="col-lg-7 col-xxl-7 p-4 row">
          <div class="mb-5">
            <h2 class="hotel-name"><!-- Hotel Name --></h2>
            <div class="room-detail-container-row row align-items-start justify-content-sm-between m-0 mb-2">
              <p id="roomSpecs" class="col-12 col-sm-auto mb-0 px-1 text-truncate"><!-- Room Specs --></p>
              <button id="roomSpecsToggleBtn" class="btn col-12 col-sm-auto">View <span>More</span></button>
            </div>
            <div class="room-detail-container-row row align-items-center">
              <div class="row align-items-center w-auto me-5">
                <span class="material-icon material-icons-round w-auto">people</span>
                <span id="maxPerson" class="icon-text w-auto"><!-- Room Max Person Limit --></span>
              </div>
              <div class="row align-items-center w-auto">
                <span class="material-icon material-icons-round w-auto">hotel</span>
                <span id="roomSize" class="icon-text w-auto"><!-- Room Size --></span>
              </div>
            </div>
          </div>
          <div class="discount-list-container row align-items-center mt-5">
            <span class="material-icons-round w-auto">discount</span>
            <div id="discountList" class="col"><!-- Discounts List --></div>
          </div>
        </div>
      </div>
      <div class="room-list-container">
        <div id="roomList" class="room-list-content"><!-- Room List --></div>
        <button id="roomsContainerToggleBtn" class="btn w-100"><span>Hide</span> All</button>
      </div>
    </div>
  `,
  // Room Image
  ROOM_IMAGE_CONTAINER_SELECTOR: "#roomImage",
  HOTEL_ROOM_REMAIN_SELECTOR: "#roomsRemain",
  // Hotel Room Details
  HOTEL_NAME_SELECTOR: ".hotel-name",
  HOTEL_ROOM_SPECS_CONTAINER_SELECTOR: "#roomSpecs",
  HOTEL_ROOM_SPECS_VIEW_MORE_BUTTON_SELECTOR: "#roomSpecsToggleBtn",
  HOTEL_ROOM_PERSON_LIMIT_SELECTOR: "#maxPerson",
  HOTEL_ROOM_SIZE_SELECTOR: "#roomSize",
  // Discount List
  HOTEL_ROOM_DISCOUNT_TEMPLATE: `
    <button class="btn discount-btn">
      <span class="discount-name"><!-- Discount Name --></span>
    </button>
  `,
  HOTEL_ROOM_DISCOUNT_LIST_CONTAINER_SELECTOR: "#discountList",
  HOTEL_ROOM_DISCOUNT_BUTTON_SELECTOR: ".discount-btn",
  HOTEL_ROOM_DISCOUNT_NAME_SELECTOR: ".discount-name",
  // Rooms List
  HOTEL_ROOM_LIST_TEMPLATE: `
    <div class="room-list-content-row row mx-0">
      <div class="room-list-content-col px-0 col-12 col-sm-12 col-md-4">
        <h5 class="room-name"><!-- Room Name --></h5>
        <p class="room-details"><!-- Room Details --></p>
        <button class="btn room-detail-view-button"><span>View</span> Details</button>
      </div>
      <div class="room-list-content-col my-2 my-md-0 px-0 col-12 col-sm-6 col-md-4">
        <div class="row mx-0 my justify-content-md-center align-items-center">
          <button class="w-auto btn room-quantity-inc-btn">
            <span class="material-icons-round">add</span>
          </button>
          <span class="w-auto room-current-quantity mx-3"><!-- Selected Rooms --></span>
          <button class="w-auto btn room-quantity-dec-btn">
            <span class="material-icons-round">remove</span>
          </button>
        </div>
      </div>
      <div class="room-list-content-col my-3 my-md-0 px-0 col-12 col-sm-6 col-md-4">
        <div class="room-current-price-container mx-0 row align-items-baseline justify-content-sm-end">
          <div style="display: flex; align-self: center;" class="w-auto px-0" >
            <span class="material-icons-round">currency_rupee</span>
          </div>
          <span class="room-current-price w-auto px-0"><!-- Room Current Price --></span>
          <span class="w-auto p-0">/</span>
          <span class="room-price-per-unit w-auto px-0">night</span>
        </div>
        <div class="room-price-refundable-status-container mx-0 row align-items-center justify-content-sm-end">
          <span class="room-price-refundable-status w-auto px-0 pe-2"><!-- Refundable Status --></span>
          <span class="material-icon material-icons-round w-auto px-0">info</span>
        </div>
      </div>
    </div>
  `,
  HOTEL_ROOM_LIST_CONTAINER_SELECTOR: "#roomList",
  HOTEL_ROOM_NAME_SELECTOR: ".room-name",
  HOTEL_ROOM_DETAILS_CONTAINER_SELECTOR: ".room-details",
  HOTEL_ROOM_DETAIL_VIEW_BUTTON_SELECTOR: ".room-detail-view-button",
  HOTEL_ROOM_QUANTITY_INCREMENT_BUTTON_SELECTOR: ".room-quantity-inc-btn",
  HOTEL_ROOM_CURRENT_QUANTITY_SELECTOR: ".room-current-quantity",
  HOTEL_ROOM_QUANTITY_DECREMENT_BUTTON_SELECTOR: ".room-quantity-dec-btn",
  HOTEL_ROOM_DISCOUNT_APPLIED_INDICATOR_TEMPLATE: `
    <p class="discount-indicator text-md-center mt-3">
      <span>*Applied</span>&nbsp;<span class="discount-name"><!-- Discount Name --></span>
    </p>
  `,
  HOTEL_ROOM_DISCOUNT_APPLIED_INDICATOR_SELECTOR: ".discount-indicator",
  HOTEL_ROOM_INITIAL_PRICE_CONTAINER_TEMPLATE: `
    <div class="room-initial-price-container row align-items-center justify-content-end">
      <p class="w-auto p-0 m-0  text-decoration-line-through">
        <span class="w-auto p-0">INR</span>
        <span class="room-initial-price w-auto p-0 pe-2"><!-- Room Initial Price --></span>
      </p>
      <span class="material-icon material-icons-round w-auto p-0">info</span>
    </div>
  `,
  HOTEL_ROOM_INITIAL_PRICE_CONTAINER_SELECTOR: ".room-initial-price-container",
  HOTEL_ROOM_INITIAL_PRICE_SELECTOR: ".room-initial-price",
  HOTEL_ROOM_CURRENT_PRICE_SELECTOR: ".room-current-price",
  HOTEL_ROOM_REFUNDABLE_STATUS_SELECTOR: ".room-price-refundable-status",
  HOTEL_ROOM_LIST_VIEW_ALL_BUTTON_SELECTOR: "#roomsContainerToggleBtn",
  // Checkout Container
  CHECKOUT_CONTAINER_SELECTOR: "#checkoutContainer",
  CHECKOUT_CONTAINER_TEMPLATE: `
    <div id="checkoutContainer" class="container col-xl-4 shadow rounded-3 border mt-xl-0 px-0">
      <h2 class="mb-0" >Tarrif Details</h2>
      <div class="row align-items-center m-0">
        <div class="row align-items-center w-auto">
          <span class="w-auto p-0">Rooms</span>
          <span class="w-auto p-0 ms-3" id="totalRoomCount"><!-- Total Room Count --></span>
        </div>
        <div class="row align-items-center w-auto ms-5">
          <span class="w-auto p-0">Adults</span>
          <span class="w-auto p-0 ms-3" id="totalHeadCount"><!-- Total Head Count --></span>
        </div>
      </div>
      <div class="row justify-content-between">
        <div class="col-xxl-5 col-sm-6 row justify-content-center">
          <div class="col-12 text-center">
            <h6 class="w-auto me-1">Check-in:</h6>
            <span>10:00 A.M.</span>
          </div>
          <input
            class="datetime-input p-0"
            type="date"
            id="checkinDatetime"
            min=""
            max=""
            value=""
          />
        </div>
        <div class="col-xxl-5 col-sm-6 row justify-content-center">
          <div class="col-12 text-center">
            <h6 class="w-auto me-1">Check-out:</h6>
            <span>08:00 A.M.</span>
          </div>
          <input
            class="datetime-input p-0"
            type="date"
            id="checkoutDatetime"
            min=""
            max=""
            value=""
          />
        </div>
      </div>
      <div id="selectedRoomList"><!-- Selected Rooms List --></div>
      <div>
        <div id="priceBreakDown" style="display: none;">
          <div class="row justify-content-between">
            <span class="w-auto">Total Amount</span>
            <span class="w-auto" id="totalInitialAmount"><!-- Total Initial Amount --></span>
          </div>
          <div class="row justify-content-between">
            <span class="w-auto" >Discount Amount</span>
            <div class="w-auto" >
              <span>&minus;</span>
              <span id="totalDiscountAmount"><!-- Total Discount Amount --></span>
            </div>
          </div>
        </div>
        <div class="row justify-content-between m-0">
          <h4 class="w-auto px-0" >Total Payable</h4>
          <h4 class="w-auto px-0" >
            INR <span id="totalAmount"><!-- Total Amount --></span>
          </h4>
        </div>
        <button id="priceBreakDownToggle" class="btn"><span>View</span> All Details</button>
        <form id="checkoutForm" method="POST">
          <button disabled type="submit" id="submitCheckoutForm" class="btn">Proceed To Payment</button>
        </form>
        <p id="termsAndConditions">
          By proceeding you agree to the <a href="#">Hotel Booking Policy</a>, <a href="#">Hotel Cancellation Policy</a> and
          <a href="#">Terms &amp; Conditions</a> of <span class="hotel-name"> <!-- Hotel Name --> </span>.
        </p>
      </div>
    </div>
  `,
  CHECKOUT_DISPLAY_TOTAL_ROOM_COUNT_SELECTOR: "#totalRoomCount",
  CHECKOUT_DISPLAY_TOTAL_HEAD_COUNT_SELECTOR: "#totalHeadCount",
  CHECKOUT_INPUT_CHECKIN_DATETIME_SELECTOR: "#checkinDatetime",
  CHECKOUT_INPUT_CHECKOUT_DATETIME_SELECTOR: "#checkoutDatetime",
  CHECKOUT_SELECTED_ROOM_LIST_CONTAINER_SELECTOR: "#selectedRoomList",
  CHECKOUT_SELECTED_ROOM_ID_SUFFIX: "Checkout",
  CHECKOUT_SELECTED_ROOM_CONTAINER_TEMPLATE: `
    <div class="row mx-0 align-items-baseline justify-content-between">
      <div class="px-0 col-12 col-sm-4 col-xl-12 col-xxl-4">
        <h6>SEMI DELUXE</h6>
        <h6 class="room-name text-truncate"><!-- Room Name --></h6>
      </div>
      <div class="px-1 col-6 col-sm-4 col-xl-6 col-xxl-4 mb-1 w-auto text-center">
        <span style="font-size: 20px">&times;</span>
        <select class="update-room-current-quantity"></select>
      </div>
      <div class="px-0 col-6 col-sm-4 col-xl-6 col-xxl-4 mb-1 row mx-0 align-items-baseline justify-content-end">
        <div class="p-0 w-auto">
          <span>INR</span>
          <span class="room-current-price"><!-- Room Current Price --></span>
        </div>
        <button title="Clear this room" class="btn remove-room">&times;</button>
      </div>
    </div>
  `,
  CHECKOUT_INPUT_UPDATE_ROOM_CURRENT_QUANTITY_SELECTOR: ".update-room-current-quantity",
  CHECKOUT_INPUT_UPDATE_ROOM_CURRENT_QUANTITY_CONTAINER_TEMPLATE: `
    <option value=""><!-- Option Text --></option>
  `,
  CHECKOUT_REMOVE_ROOM_BUTTON_SELECTOR: ".remove-room",
  CHECKOUT_PRICE_BREAK_DOWN_CONTAINER_SELECTOR: "#priceBreakDown",
  CHECKOUT_PRICE_BREAK_DOWN_TOGGLE_BUTTON_SELECTOR: "#priceBreakDownToggle",
  CHECKOUT_DISPLAY_TOTAL_INITIAL_ROOM_PRICE_SELECTOR: "#totalInitialAmount",
  CHECKOUT_DISPLAY_TOTAL_DISCOUNT_AMOUNT_SELECTOR: "#totalDiscountAmount",
  CHECKOUT_DISPLAY_TOTAL_AMOUNT_SELECTOR: "#totalAmount",
  CHECKOUT_FORM_SELECTOR: "#checkoutForm",
  CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR: "#submitCheckoutForm",
  CHECKOUT_TERMS_AND_CONDITIONS_CONTAINER_SELECTOR: "#termsAndConditions"
}