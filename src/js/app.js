(() => {
  // TODO: Convert the ids in camelCase and change '__' in classes.
  
  // Local Data
  const localData = {
    animeSpeed: 300,
    roomQuantity: {},
    roomsRemaining: pageData.room.remaining,
    appliedDiscountId: "",
    totalDiscountAmount: 0,
    totalInitialRoomAmount: 0,
    formData: {
      totalRoomCount: 0,
      totalHeadCount: 0,
      checkin: {
        min: '',
        max: '',
        value: ''
      },
      checkout: {
        min: '',
        max: '',
        value: ''
      },
      totalAmount: 0
    }
  };
  const dateUtils = {
    getLocaleISODateString: (date) => {
      try {
        if (! date instanceof Date) {
          throw TypeError("passed argument should be an instance of Date.");
        }
        // Return ISO representation of locale datetime
        const yyyy = date.getFullYear();
        const MM = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
        const dd = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
        // const hh = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
        // const mm = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
        // return `${yyyy}-${MM}-${dd}T${hh}:${mm}`;
        return `${yyyy}-${MM}-${dd}`;
      } catch (error) {
        console.error(error);
      }
    },
    getDifference: function (checkinDate ,checkoutDate) {
      try {
        checkinDate = new Date(checkinDate);
        checkoutDate = new Date(checkoutDate);
        if ( isNaN(checkinDate) || isNaN(checkoutDate)) {
          throw Error("Invalid Date");
        }
        const diff = {
          days: 0,
        }
        const monthDiff = checkoutDate.getMonth() - checkinDate.getMonth();
        if (monthDiff > 0) {
          // checkinDate < checkoutDate
          const lastDateOfMonth = new Date(checkinDate.getFullYear(), checkinDate.getMonth()+1, 0).getDate();
          diff.days = lastDateOfMonth - checkinDate.getDate();
          for (let mon = 1; mon <= monthDiff; mon++) {
            if (mon === monthDiff) {
              diff.days += checkoutDate.getDate();
            } else {
              diff.days += new Date(checkinDate.getFullYear(), checkoutDate.getMonth()+mon+1, 0).getDate();
            }
          }
        } else if (monthDiff < 0) {
          // checkinDate > checkoutDate
          const lastDateOfMonth = new Date(checkoutDate.getFullYear(), checkoutDate.getMonth()+1, 0).getDate();
          diff.days = lastDateOfMonth - checkoutDate.getDate();
          for (let mon = monthDiff; mon < 0; mon++) {
            if (mon === monthDiff) {
              diff.days += checkoutDate.getDate();
            } else {
              diff.days += new Date(checkinDate.getFullYear(), checkinDate.getMonth()+mon+1, 0).getDate();
            }
          }
          diff.days = -(diff.days);
        } else {
          // checkinDate == checkoutDate
          diff.days = checkoutDate.getDate() - checkinDate.getDate();
        }
        return diff;
      } catch (error) {
        console.error(error);
      }
    },
  }
  const convertor = {
    toTitleCase: (str) => {
      try {
        return str.toLowerCase().replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
      } catch (error) {
        console.error(error);
      }
    },
    priceToDisplay: (price) => {
      try {
        return new Intl.NumberFormat('en-IN').format(price);
      } catch (error) {
        console.error(error);
      }
    },
  }
  
  // Updating through an object
  const updateHtml = {
    container: $(CONSTANTS.CONTAINER_TEMPLATE),
    checkoutContainer: $(CONSTANTS.CHECKOUT_CONTAINER_TEMPLATE),
    ready: function () {
      $(CONSTANTS.MAIN_CONTAINER_SELECTOR).html(this.container);
    },
    roomImageHandler: function () {
      try {
        this.container.find(CONSTANTS.ROOM_IMAGE_CONTAINER_SELECTOR).css("background-image", `url(${pageData.room.imageUrl})`);
      } catch (error) {
        console.error(error);
      }
    },
    roomsRemainingHandler: function () {
      try {
        if (localData.roomsRemaining > 1) {
          this.container.find(CONSTANTS.HOTEL_ROOM_REMAIN_SELECTOR).text(`${localData.roomsRemaining} rooms left`);
        } else if (localData.roomsRemaining === 1) {
          this.container.find(CONSTANTS.HOTEL_ROOM_REMAIN_SELECTOR).text(`${localData.roomsRemaining} room left`);
        } else if (localData.roomsRemaining === 0) {
          this.container.find(CONSTANTS.HOTEL_ROOM_REMAIN_SELECTOR).text("Fully Booked");
        } else {
          throw Error("No Data Found regarding room remaining.")
        }
      } catch (error) {
        console.error(error);
      }
    },
    hotelNameHandler: function () {
      try {
        this.container.find(CONSTANTS.HOTEL_NAME_SELECTOR).text(pageData.hotelName);
      } catch (error) {
        console.error(error);
      }
    },
    roomSpecsHandler: function (trunc=true) {
      try {
        if (!trunc) {
          this.container.find(CONSTANTS.HOTEL_ROOM_SPECS_CONTAINER_SELECTOR).html(pageData.room.specs.join("<span>|</span>"));
          return;
        }
        const stringLength = pageData.room.specs.reduce((total, str) => {return total + str.length ;} ,0);
        let truncLength = (stringLength * 0.7) > 60 ? 60 : Math.round(stringLength * 0.7);
        const list = pageData.room.specs.filter((str) => {
          // also, if tl > 0 && tl-sl < 0
          //       => do tl -= sl
          //          and return str[0:sl/2]
          truncLength -= str.length;
          if (truncLength > 0) return str;
        });
        let value = list.join("<span>|</span>");
        value += "<span>|</span>..."
        this.container.find(CONSTANTS.HOTEL_ROOM_SPECS_CONTAINER_SELECTOR).html(value);
      } catch (error) {
        console.error(error);
      }
    },
    roomSpecsToggleEventHandler: function () {
      try {
        const para = this.container.find(CONSTANTS.HOTEL_ROOM_SPECS_CONTAINER_SELECTOR);
        const btn = this.container.find(CONSTANTS.HOTEL_ROOM_SPECS_VIEW_MORE_BUTTON_SELECTOR);
        const span = btn.children("span");
        btn.click(() => {
          if (span.text().toLowerCase() === "more") {
            this.roomSpecsHandler(false);
            span.text("Less");
            para.removeClass("text-truncate");
          } else {
            this.roomSpecsHandler(true);
            span.text("More");
            para.addClass("text-truncate");
          }
        });
      } catch (error) {
        console.error(error);
      }
    },
    personLimitHandler: function () {
      try {
        this.container.find(CONSTANTS.HOTEL_ROOM_PERSON_LIMIT_SELECTOR).text(pageData.room.maxPerson);
      } catch (error) {
        console.error(error);
      }
    },
    roomSizeHandler: function () {
      try {
        this.container.find(CONSTANTS.HOTEL_ROOM_SIZE_SELECTOR).text(`${pageData.room.size} Size`);
      } catch (error) {
        console.error(error);
      }
    },
    discountListHandler: function () {
      try {
        const list = [];
        for (let discount of pageData.discounts) {
          const btn = $(CONSTANTS.HOTEL_ROOM_DISCOUNT_TEMPLATE);
          btn.attr("id", discount.name);
          btn.find(CONSTANTS.HOTEL_ROOM_DISCOUNT_NAME_SELECTOR).text(discount.name);
          list.push(btn);
          list.push(", ");
        }
        list.pop();
        this.container.find(CONSTANTS.HOTEL_ROOM_DISCOUNT_LIST_CONTAINER_SELECTOR).append(...list);
      } catch (error) {
        console.error(error);
      }
    },
    discountButtonsEventHandler: function () {
      for (let discount of pageData.discounts) {
        this.container.find(`#${discount.name}`).click((event) => {
          if (event.currentTarget.id === localData.appliedDiscountId) {
            event.currentTarget.classList.toggle("applied");
            localData.appliedDiscountId = "";
          } else {
            if (localData.appliedDiscountId) {
              $(`#${localData.appliedDiscountId}`).removeClass("applied");
            }
            localData.appliedDiscountId = event.currentTarget.id;
            event.currentTarget.classList.add("applied");
          }
          this.discountIndicatorHandler();
          this.handleRoomPriceChange();
          this.roomInitialPriceHandler();
          // Update and Display total discount value
          this.totalDiscountAmountHandler();
          // Update and Display total amount value
          this.totalAmountHandler();
        });
      }
    },
    handleDiscountAmount: function (discountData, room, onlyDiscount=false) {
      try {
        if (discountData.rateInPercent) {
          if (onlyDiscount) {
            return (discountData.rate / 100) * room.price;
          } else {
            return room.price - ((discountData.rate / 100) * room.price);
          }
        } else {
          if (onlyDiscount) {
            return discountData.rate;
          } else {
            return room.price > discountData.rate ? room.price - discountData.rate : 0;
          }
        }
      } catch (error) {
        console.error(error);
      }
    },
    handleRoomPriceChange: function () {
      try {
        let discountData;
        if (localData.appliedDiscountId) {
          discountData = pageData.discounts.filter((discount) => discount.name === localData.appliedDiscountId)[0];
        }
        for (const room of pageData.room.types) {
          this.container.find(`#${room.id} ${CONSTANTS.HOTEL_ROOM_CURRENT_PRICE_SELECTOR}`).text(convertor.priceToDisplay(room.price));
          if (localData.appliedDiscountId && room.discountApplicable) {
            // Display discounted price
            const discountedPrice = this.handleDiscountAmount(discountData, room);
            this.container.find(`#${room.id} ${CONSTANTS.HOTEL_ROOM_CURRENT_PRICE_SELECTOR}`).text(convertor.priceToDisplay(discountedPrice));
          }
        }
      } catch (error) {
        console.error(error);
      }
    },
    roomListHandler: function () {
      try {
        const roomsList = [];
        for (let room of pageData.room.types) {
          localData.roomQuantity[room.id] = 0;
          const roomObj = $(CONSTANTS.HOTEL_ROOM_LIST_TEMPLATE);
          // Add id
          roomObj.attr("id", room.id);
          // Add name
          roomObj.find(CONSTANTS.HOTEL_ROOM_NAME_SELECTOR).text(room.name);
          // Add details
          roomObj.find(CONSTANTS.HOTEL_ROOM_DETAILS_CONTAINER_SELECTOR).text(room.details).hide(localData?.animeSpeed);
          // Add current room quantity
          roomObj.find(CONSTANTS.HOTEL_ROOM_CURRENT_QUANTITY_SELECTOR).text(localData?.roomQuantity[room.id]);
          // Add initial room price
          roomObj.find(CONSTANTS.HOTEL_ROOM_CURRENT_PRICE_SELECTOR).text(convertor.priceToDisplay(room.price));
          // Add refundable status
          roomObj.find(CONSTANTS.HOTEL_ROOM_REFUNDABLE_STATUS_SELECTOR).text(room.refundable ? "Refundable": "Non Refundable");
          // Push roomObj to list
          roomsList.push(roomObj);
        }
        this.container.find(CONSTANTS.HOTEL_ROOM_LIST_CONTAINER_SELECTOR).html(roomsList);
      } catch (error) {
        console.error(error);
      }
    },
    discountIndicatorHandler: function () {
      try {
        for (let room of pageData.room.types) {
          // Remove discount indicator
          this.container.find(`#${room.id} ${CONSTANTS.HOTEL_ROOM_DISCOUNT_APPLIED_INDICATOR_SELECTOR}`).remove();
          if (localData.appliedDiscountId && room.discountApplicable) {
            // Add discount indicator
            const content = $(CONSTANTS.HOTEL_ROOM_DISCOUNT_APPLIED_INDICATOR_TEMPLATE);
            content.children(CONSTANTS.HOTEL_ROOM_DISCOUNT_NAME_SELECTOR).text(localData.appliedDiscountId);
            this.container.find(`#${room.id} > :nth-child(2)`).append(content);
          }
        }
      } catch (error) {
        console.error(error);
      }
    },
    roomInitialPriceHandler: function () {
      try {
        for (let room of pageData.room.types) {
          this.container.find(`#${room.id} ${CONSTANTS.HOTEL_ROOM_INITIAL_PRICE_CONTAINER_SELECTOR}`).remove();
          if (localData.appliedDiscountId && room.discountApplicable) {
            const content = $(CONSTANTS.HOTEL_ROOM_INITIAL_PRICE_CONTAINER_TEMPLATE);
            content.find(CONSTANTS.HOTEL_ROOM_INITIAL_PRICE_SELECTOR).text(convertor.priceToDisplay(room.price));
            this.container.find(`#${room.id} > :last-child`).prepend(content);
          }
        }
      } catch (error) {
        console.error(error);
      }
    },
    handleRoomQuantityChange: function (room, isInc) {
      try {
        if (isInc) {
          // Increment Logic
          if (localData.roomsRemaining > 0 && localData.roomQuantity[room.id] < room.maxQuantity) {
            localData.roomsRemaining --;
            localData.roomQuantity[room.id] ++;
            // Update Container Total Room Details
            this.roomsRemainingHandler();
            // Update Checkout Container
            this.checkoutUpdateHandler();
          } 
          // Checkout Container Insert Logic
          if (localData.roomsRemaining === pageData.room.remaining-1) {
            this.checkoutListenerEventsHandler();
            $(CONSTANTS.MAIN_CONTAINER_SELECTOR).append(this.checkoutContainer);
            this.container.addClass('col-xl-7');
            this.container.removeClass('col-xxl-10');
            this.container.find(CONSTANTS.ROOM_IMAGE_CONTAINER_SELECTOR).addClass("col-xl-12").next().addClass("col-xl-12");
          }
        } else {
          // Decrement Logic
          if (localData.roomQuantity[room.id] > 0) {
            localData.roomQuantity[room.id] --;
            localData.roomsRemaining ++;
            // Update Container Total Room Details
            this.roomsRemainingHandler();
            // Update Checkout Container
            this.checkoutUpdateHandler();
          }
          // Check out container Remove Logic
          if (localData.roomsRemaining === pageData.room.remaining) {
            this.checkoutListenerEventsHandler(true);
            $(CONSTANTS.CHECKOUT_CONTAINER_SELECTOR).remove();
            this.container.removeClass('col-xl-7');
            this.container.addClass('col-xxl-10');
            this.container.find(CONSTANTS.ROOM_IMAGE_CONTAINER_SELECTOR).removeClass("col-xl-12").next().removeClass("col-xl-12");
            localData.formData.checkin.value = '';
            localData.formData.checkout.value = '';
          }
        }
        
        //! // Update checkout container room details
        // if (localData.roomQuantity[roomId] > 0) {
        //   this.checkoutContainer.find(`#${roomId}${CONSTANTS.CHECKOUT_SELECTED_ROOM_ID_SUFFIX} ${CONSTANTS.HOTEL_ROOM_CURRENT_QUANTITY_SELECTOR}`).text(localData.roomQuantity[roomId]);
        // } else {
        //   // Remove the event listener to the .remove-room button
        //   this.checkoutContainer.find(`#${roomId}${CONSTANTS.CHECKOUT_SELECTED_ROOM_ID_SUFFIX} ${CONSTANTS.CHECKOUT_REMOVE_ROOM_BUTTON_SELECTOR}`).off();
        //   // Remove the room container
        //   this.checkoutContainer.find(`#${roomId}${CONSTANTS.CHECKOUT_SELECTED_ROOM_ID_SUFFIX}`).remove();
        // }
        
        // Display 
        this.container.find(`#${room.id} ${CONSTANTS.HOTEL_ROOM_CURRENT_QUANTITY_SELECTOR}`).text(localData.roomQuantity[room.id]);
      } catch (error) {
        console.error(error);
      }
    },
    roomListElementsEventHandler: function () {
      try {
        for (const room of pageData.room.types) {
          const container = this.container.find(`#${room.id}`);
          const detail = container.find(CONSTANTS.HOTEL_ROOM_DETAILS_CONTAINER_SELECTOR);
          const viewDetailBtn = container.find(CONSTANTS.HOTEL_ROOM_DETAIL_VIEW_BUTTON_SELECTOR);
          const detailSpan = viewDetailBtn.children("span");
          const incBtn = container.find(CONSTANTS.HOTEL_ROOM_QUANTITY_INCREMENT_BUTTON_SELECTOR);
          const decBtn = container.find(CONSTANTS.HOTEL_ROOM_QUANTITY_DECREMENT_BUTTON_SELECTOR);
          
          // Add Event Listener to Detail Button
          viewDetailBtn.click(() => {
            detail.slideToggle(localData.animeSpeed);
            if (detailSpan.text().toLowerCase() === "view") detailSpan.text("Hide");
              else detailSpan.text("View");
          });
          // Add Event Listener to Increment Button
          incBtn.click(() => {
            this.handleRoomQuantityChange(room, true);
          });
          // Add Event Listener to Decrement Button
          decBtn.click(() => {
            this.handleRoomQuantityChange(room, false);
          });
        }
      } catch (error) {
        console.error(error);
      }
    },
    roomListToggleEventHandler: function () {
      this.container.find(CONSTANTS.HOTEL_ROOM_LIST_VIEW_ALL_BUTTON_SELECTOR).click((event) => {
        this.container.find(CONSTANTS.HOTEL_ROOM_LIST_CONTAINER_SELECTOR).slideToggle();
        if (event.currentTarget.children[0].innerText.toLowerCase() === "view") {
          event.currentTarget.children[0].innerText = "Hide";
        } else {
          event.currentTarget.children[0].innerText = "View";
        }
      });
    },
    listenerEventsHandler: function () {
      this.roomSpecsToggleEventHandler();
      this.roomListElementsEventHandler();
      this.discountButtonsEventHandler();
      this.roomListToggleEventHandler();
    },
    containerSetup: function () {
      try {
        //*   1. Update Room Image
        this.roomImageHandler();
        //*   2. Update Remaining Rooms Value
        this.roomsRemainingHandler();
        //*   3. Update Hotel Name
        this.hotelNameHandler();
        //*   4. Update Hotel Room Specs
        this.roomSpecsHandler();
        //*   5. Update Hotel Room Max Person Limit
        this.personLimitHandler();
        //*   6. Update Hotel Room Size
        this.roomSizeHandler();
        //*   7. Update Discount List
        this.discountListHandler();
        //*   8. Update Rooms List
        this.roomListHandler();
        //*   9. Add Event Listeners
        this.listenerEventsHandler();
      
      } catch (error) {
        console.error(error);
      }
    },
    handleTotalRoomAndHeadCount: function () {
      try {
        localData.formData.totalRoomCount = pageData.room.remaining - localData.roomsRemaining;
        localData.formData.totalHeadCount = localData.formData.totalRoomCount * pageData.room.maxPerson;
      } catch (error) {
        console.error(error);
      }
    },
    handleRoomsTotalInitialAmount: function () {
      try {
        const roomIds = Object.keys(localData.roomQuantity).filter((roomId) => {
          if (localData.roomQuantity[roomId] > 0) {
            return roomId;
          }
        });
        localData.totalInitialRoomAmount = pageData.room.types.reduce((total,room) => {
          if (roomIds.includes(room.id)) {
            return total + (room.price * localData.roomQuantity[room.id]);
          }
          return total;
        }, 0);
        if (localData.formData.checkin.value && localData.formData.checkout.value) {
          localData.totalInitialRoomAmount *= dateUtils.getDifference(localData.formData.checkin.value, localData.formData.checkout.value).days;
        }
      } catch (error) {
        console.error(error);
      }
    },
    handleTotalDiscountAmount: function () {
      try {
        let discountData;
        if (localData.appliedDiscountId) {
          discountData = pageData.discounts.filter((discount) => discount.name === localData.appliedDiscountId)[0];
        }
        localData.totalDiscountAmount = pageData.room.types.reduce((total, room) => {
          if (localData.appliedDiscountId && room.discountApplicable) {
            let discountAmount = this.handleDiscountAmount(discountData, room, true) * localData.roomQuantity[room.id];
            if (localData.formData.checkin.value && localData.formData.checkout.value) {
              discountAmount *= dateUtils.getDifference(localData.formData.checkin.value, localData.formData.checkout.value).days;
            }
            return total + discountAmount;
          }
          return total;
        }, 0);
      } catch (error) {
        console.error(error);
      }
    },
    handleTotalAmount: function () {
      try {
        localData.formData.totalAmount = localData.totalInitialRoomAmount - localData.totalDiscountAmount;
      } catch (error) {
        console.error(error);
      }
    },
    totalRoomAndHeadCountHandler: function () {
      try {
        // Update
        this.handleTotalRoomAndHeadCount();
        // Display
        this.checkoutContainer.find(CONSTANTS.CHECKOUT_DISPLAY_TOTAL_ROOM_COUNT_SELECTOR).text(localData.formData.totalRoomCount);
        this.checkoutContainer.find(CONSTANTS.CHECKOUT_DISPLAY_TOTAL_HEAD_COUNT_SELECTOR).text(localData.formData.totalHeadCount);
      } catch (error) {
        console.error(error);
      }
    },
    // handleCheckinDate: function (checkinDateValue) {
    //   try {
    //     const varDate = new Date(checkinDateValue);
    //     if (isNaN(varDate)) {
    //       throw Error("Invalid Date");
    //     }
    //     const date = dateUtils.getLocaleISODateString;
        
    //   } catch (error) {
    //     console.error(error);
    //   }
    // },
    handleCheckoutDate: function (checkinDate) {
      try {
        // Checkout => Value tomorrow, Min tomorrow, Max (today + 7 days
        const varDate = new Date(checkinDate);
        if (isNaN(varDate)) {
          throw Error("Invalid Date");
        }
        const date = dateUtils.getLocaleISODateString;
        const checkout = {
          value: "",
          min: "",
          max: "",
        };
        // Set Timing as well
        varDate.setDate(varDate.getDate() + 1);
        checkout.value = date(varDate);
        checkout.min = date(varDate);
        varDate.setDate(varDate.getDate() + 6);
        checkout.max = date(varDate);
        return checkout;
      } catch (error) {
        console.error(error);
      }
    },
    handleCheckinCheckoutDate: function () {
      try {
        const checkin = {
          value: "",
          min: "",
          max: "",
        };
        const date = dateUtils.getLocaleISODateString;
        const today = new Date();
        // Checkin => Value today, Min today, Max 1 month
        checkin.value = date(today);
        checkin.min = date(today);
        const varDate = new Date(today);
        varDate.setMonth(varDate.getMonth() + 1);
        checkin.max = date(varDate);
        const checkout = this.handleCheckoutDate(today);
        localData.formData.checkin = checkin;
        localData.formData.checkout = checkout;
        return {checkin, checkout};
      } catch (error) {
        console.error(error);
      }
    },
    checkinCheckoutHandler: function () {
      try {
        const checkin = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKIN_DATETIME_SELECTOR);
        const checkout = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKOUT_DATETIME_SELECTOR);
        if (localData.formData.checkin.value && localData.formData.checkout.value) {
          checkin.attr(localData.formData.checkin);
          checkin.val(localData.formData.checkin.value);
          checkout.attr(localData.formData.checkout);
          checkout.val(localData.formData.checkout.value);
        } else {
          const dates = this.handleCheckinCheckoutDate();
          checkout.attr(dates.checkout);
          checkout.val(dates.checkout.value);
          checkin.attr(dates.checkin);
          checkin.val(dates.checkin.value);
        }
        this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).removeAttr("disabled");
      } catch (error) {
        console.error(error);
      }
    },
    checkoutRoomHandler: function () {
      try {
        const container = this.checkoutContainer.find(CONSTANTS.CHECKOUT_SELECTED_ROOM_LIST_CONTAINER_SELECTOR);
        const roomIds = Object.keys(localData.roomQuantity).filter((roomId) => {
          if (localData.roomQuantity[roomId] > 0) {
            return roomId;
          }
        });
        let discountData;
        if (localData.appliedDiscountId) {
          discountData = pageData.discounts.filter((discount) => discount.name === localData.appliedDiscountId)[0];
        }
        const roomsList = [];
        for (const room of pageData.room.types) {
          if (roomIds.includes(room.id)) {
            const roomData = $(CONSTANTS.CHECKOUT_SELECTED_ROOM_CONTAINER_TEMPLATE);
            roomData.attr("id", `${room.id}Checkout`);
            // Update Room Name (only upto 9 characters and three dots at end if content exceeds 12 characters) //! Not for bootstrap
            // roomData.find(CONSTANTS.HOTEL_ROOM_NAME_SELECTOR).text(room.name.length > 12 ? room.name.substring(0,9) + '...' : room.name).attr('data-text-content', room.name);
            roomData.find(CONSTANTS.HOTEL_ROOM_NAME_SELECTOR).text(room.name);
            // Update Room Current Quantity
            // roomData.find(CONSTANTS.HOTEL_ROOM_CURRENT_QUANTITY_SELECTOR).text(localData.roomQuantity[room.id]);
            const selectOptions = []
            for (let i=1; i<=room.maxQuantity; i++) {
              if ((localData.roomsRemaining + localData.roomQuantity[room.id]) - i < 0) {
                continue;
              }
              const option = $(CONSTANTS.CHECKOUT_INPUT_UPDATE_ROOM_CURRENT_QUANTITY_CONTAINER_TEMPLATE);
              option.removeAttr("selected");
              if (i === localData.roomQuantity[room.id]) {
                option.attr("selected", true);
              }
              // option.removeAttr("disabled");
              // if (localData.roomsRemaining - i < 0) {
              //   option.attr("disabled", true);
              // }
              option.val(i);
              option.text(i==1 ? `${i} Room` : `${i} Rooms`);
              selectOptions.push(option);
            }
            roomData.find(CONSTANTS.CHECKOUT_INPUT_UPDATE_ROOM_CURRENT_QUANTITY_SELECTOR).html(selectOptions).change((event) => {
              const num = parseInt(event.target.value);
              if (num > localData.roomQuantity[room.id]) {
                localData.roomsRemaining += localData.roomQuantity[room.id];
                localData.roomQuantity[room.id] = num;
                localData.roomsRemaining -= num;
                localData.roomQuantity[room.id]--;
                localData.roomsRemaining++;
                this.handleRoomQuantityChange(room, true);
              } else {
                localData.roomsRemaining += localData.roomQuantity[room.id];
                localData.roomQuantity[room.id] = num;
                localData.roomsRemaining -= num;
                localData.roomQuantity[room.id]++;
                localData.roomsRemaining--;
                this.handleRoomQuantityChange(room, false);
              }
            });
            // Update Room Current Price
            roomData.find(CONSTANTS.HOTEL_ROOM_CURRENT_PRICE_SELECTOR).text(convertor.priceToDisplay(room.price));
            // roomData.find(CONSTANTS.HOTEL_ROOM_CURRENT_PRICE_SELECTOR).text(convertor.priceToDisplay(room.price));
            // if (localData.appliedDiscountId && room.discountApplicable) {
            //   const discountedPrice = this.handleDiscountAmount(discountData, room);
            //   roomData.find(CONSTANTS.HOTEL_ROOM_CURRENT_PRICE_SELECTOR).text(convertor.priceToDisplay(discountedPrice));
            // }
            //? Full remove/reset logic of room data
            roomData.find(CONSTANTS.CHECKOUT_REMOVE_ROOM_BUTTON_SELECTOR).click(() => {
              localData.roomsRemaining += localData.roomQuantity[room.id] - 1;
              localData.roomQuantity[room.id] = 1;
              this.handleRoomQuantityChange(room);
              roomData.off();
              roomData.remove();
            });
            roomsList.push(roomData);
          }
        }
        container.html(roomsList);
      } catch (error) {
        console.error(error);
      }
    },
    totalInitialRoomAmountHandler: function () {
      try {
        // Update
        this.handleRoomsTotalInitialAmount();
        // Display
        this.checkoutContainer.find(CONSTANTS.CHECKOUT_DISPLAY_TOTAL_INITIAL_ROOM_PRICE_SELECTOR).text(convertor.priceToDisplay(localData.totalInitialRoomAmount));
      } catch (error) {
        console.error(error);
      }
    },
    totalDiscountAmountHandler: function () {
      try {
        // Update
        this.handleTotalDiscountAmount();
        // Display
        this.checkoutContainer.find(CONSTANTS.CHECKOUT_DISPLAY_TOTAL_DISCOUNT_AMOUNT_SELECTOR).text(convertor.priceToDisplay(localData.totalDiscountAmount));
      } catch (error) {
        console.error(error);
      }
    },
    totalAmountHandler: function () {
      try {
        // Update
        this.handleTotalAmount();
        // Display
        this.checkoutContainer.find(CONSTANTS.CHECKOUT_DISPLAY_TOTAL_AMOUNT_SELECTOR).text(convertor.priceToDisplay(localData.formData.totalAmount));
      } catch (error) {
        console.error(error);
      }
    },
    termsAndConditionsHandler: function () {
      try {
        this.checkoutContainer.find(`${CONSTANTS.CHECKOUT_TERMS_AND_CONDITIONS_CONTAINER_SELECTOR} ${CONSTANTS.HOTEL_NAME_SELECTOR}`).text(pageData.hotelName);
      } catch (error) {
        console.error(error);
      }
    },
    checkinDateEventHandler: function (remove=false) {
      try {
        const checkin = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKIN_DATETIME_SELECTOR);
        const checkout = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKOUT_DATETIME_SELECTOR);
        if (remove) {
          checkin.off();
        } else {
          checkin.change(() => {
            this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).removeAttr('disabled');
            const today= new Date();
            const checkinDate = new Date(checkin.val());
            today.setHours(checkinDate.getHours());
            today.setMinutes(checkinDate.getMinutes());
            today.setSeconds(checkinDate.getSeconds());
            today.setMilliseconds(checkinDate.getMilliseconds());
            if (checkinDate < today) {
              this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).attr('disabled', true);
            }
            const checkoutAttr = this.handleCheckoutDate(checkin.val());
            checkout.attr(checkoutAttr);
            checkout.val(checkoutAttr.value);
            localData.formData.checkin.value = checkin.val();
            localData.formData.checkout = checkoutAttr;
            //*   1. Update Total Initial Amount
            this.totalInitialRoomAmountHandler();
            //*   2. Update Total Discount Amount
            this.totalDiscountAmountHandler();
            //*   3. Update Total Amount
            this.totalAmountHandler();
          });
        }
      } catch (error) {
        console.error(error);
      }
    },
    checkoutDateEventHandler: function (remove=false) {
      try {
        const checkout = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKOUT_DATETIME_SELECTOR);
        if (remove) {
          checkout.off();
        } else {
          checkout.change(() => {
            if (this.isDateDiffOK()) {
              localData.formData.checkout.value = checkout.val();
              //*   1. Update Total Initial Amount
              this.totalInitialRoomAmountHandler();
              //*   2. Update Total Discount Amount
              this.totalDiscountAmountHandler();
              //*   3. Update Total Amount
              this.totalAmountHandler();
              this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).removeAttr('disabled');
            } else {
              this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).attr('disabled', true);
            }
          });
        }
      } catch (error) {
        console.error(error);
      }
    },
    priceBreakDownToggleEventHandler: function (remove=false) {
      try {
        const btn = this.checkoutContainer.find(CONSTANTS.CHECKOUT_PRICE_BREAK_DOWN_TOGGLE_BUTTON_SELECTOR);
        if (remove) {
          btn.off();
        } else {
          btn.click(() => {
            this.checkoutContainer.find(CONSTANTS.CHECKOUT_PRICE_BREAK_DOWN_CONTAINER_SELECTOR).slideToggle(localData.animeSpeed);
            const span = btn.children('span');
            if (span.text().toLowerCase() === 'view') {
              span.text('Hide');
            } else {
              span.text('View');
            }
          });
        }
      } catch (error) {
        console.error(error);
      }
    },
    isDateDiffOK: function () {
      try {
        // Check Every Thing Here
        const checkin = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKIN_DATETIME_SELECTOR);
        const today= new Date();
        today.setHours(0);
        today.setSeconds(0);
        if (new Date(checkin.val()) < today) {
          return false;
        }
        const checkout = this.checkoutContainer.find(CONSTANTS.CHECKOUT_INPUT_CHECKOUT_DATETIME_SELECTOR);
        const diffObj = dateUtils.getDifference(checkin.val(), checkout.val());
        if (diffObj.days > 0 && diffObj?.days < 8) {
          return true;
        }
        return false;
      } catch (error) {
        console.error(error);
      }
    },
    checkoutSubmitEventHandler: function (remove=false) {
      try {
        if (remove) {
          this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).off();
        } else {
          this.checkoutContainer.find(CONSTANTS.CHECKOUT_FORM_SUBMIT_BUTTON_SELECTOR).click((event) => {
            event.preventDefault();
            const msg = this.isDateDiffOK();
            if (msg === true) {
              alert("Form can be submitted");
              const formData = new FormData();
              formData.set("total_rooms", localData.formData.totalRoomCount);
              formData.set("total_heads", localData.formData.totalHeadCount);
              formData.set("rooms_booked", JSON.stringify(localData.roomQuantity));
              formData.set("checkin_date", localData.formData.checkin.value);
              formData.set("checkout_date", localData.formData.checkout.value);
              formData.set("total_amount", localData.formData.totalAmount);
              formData.set("discount_id", localData.appliedDiscountId);
              console.log(...formData);
            } else {
              alert("Error Message can be displayed here");
            }
          });
        }
      } catch (error) {
        console.error(error);
      }
    },
    checkoutListenerEventsHandler: function (remove=false) {
      try {
        if (remove) {
          this.checkinDateEventHandler(true);
          this.checkoutDateEventHandler(true);
          this.priceBreakDownToggleEventHandler(true);
          this.checkoutSubmitEventHandler(true);
        }
        else {
          this.checkinDateEventHandler();
          this.checkoutDateEventHandler();
          this.priceBreakDownToggleEventHandler();
          this.checkoutSubmitEventHandler();
        }
      } catch (error) {
        console.error(error);
      }
    },
    checkoutUpdateHandler: function () {
      try {
        //*   1. Update Total Room and Head Count
        this.totalRoomAndHeadCountHandler();
        //*   2. Handle Time and Date data
        this.checkinCheckoutHandler();
        //*   2. Update Room List
        this.checkoutRoomHandler();
        //*   3. Update Total Initial Amount
        this.totalInitialRoomAmountHandler();
        //*   4. Update Total Discount Amount
        this.totalDiscountAmountHandler();
        //*   5. Update Total Amount
        this.totalAmountHandler();
        //*   6. Update Hotel Name
        this.termsAndConditionsHandler();
      } catch (error) {
        console.error(error);
      }
    },
    checkoutSetup: function () {
      try {
        //*   1. Add Update Handler
        this.checkoutUpdateHandler();
        //? NOTE: checkout events are added and removed in 'handleRoomQuantityChange'
      } catch (error) {
        console.error(error);
      }
    },
  }
  
  try {
    updateHtml.containerSetup();
    // updateHtml.checkoutSetup();
    
    // Main
    $(document).ready(function () {
      updateHtml.ready();
    });
    
  } catch (error) {
    console.error(error);
  }
  
  
  
  
  
  // /**
  //  * Fetched Data Validation Function
  //  * @param {Object} data Data object
  //  * @returns {Object} Object
  //  */
  // function fetchedDataValidation(data) {
  //   const obj = {
  //     valid: false,
  //     errors: [],
  //     data
  //   }
  //   if (!data || typeof data !== "object" || data instanceof Array) {
  //     obj.errors.push("Data is required. It can neither be null nor undefined. It must be of type 'object'.");
  //     return obj;
  //   }
  //   if (!data.hotelName || typeof data.hotelName !== "string") {
  //     obj.errors.push("Hotel name is required. It should be of type 'string'.");
  //   }
  //   if (!obj.errors.length) {
  //     obj.valid = true;
  //   }
  //   return obj;
  // }
})();